import os
import pickle

import numpy as np
import torch
import random
from sklearn.cluster import KMeans
from sklearn.metrics import confusion_matrix, classification_report
from sklearn.tree import DecisionTreeClassifier

from nids_blackbox_data_generator import get_training_data, dataset_test

debug = False
method = 'knn_dt'
label_ratio = 1.0
cluster_centroid_ratio = 50

np.random.seed(12345)
torch.manual_seed(12345)

random.seed(12345)

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

total_dataset, labeled_dataset, unlabeled_dataset = get_training_data(label_ratio=label_ratio)
cat_dict = total_dataset.get_cat_dict()
print(cat_dict)
test_dataset = dataset_test(cat_dict)

num_data = total_dataset.get_x()
labels = total_dataset.get_y()
original_labels = total_dataset.get_original_y()

total_original_label_counts = dict()
distinct_labels, distinct_label_counts = np.unique(labels, return_counts=True)

for i in range(len(distinct_labels)):
    if distinct_labels[i] != -1:
        total_original_label_counts[distinct_labels[i]] = distinct_label_counts[i]


def tree_work(load_cluster_from_file=False):
    if load_cluster_from_file:
        clustering = pickle.load(
            file=open(os.path.join('clustering', 'nslkdd' + "_" + str(cluster_centroid_ratio) + ".pkl"), 'rb'))

        cluster_assignment = clustering.predict(num_data)
    else:

        print("Beginning K-Means Clustering.")
        print("Cluster Centroid Ratio = 1/{}".format(cluster_centroid_ratio))
        print("Total No. of Centroids = {}".format(int(total_dataset.__len__() / cluster_centroid_ratio)))

        clustering = KMeans(n_clusters=int(total_dataset.__len__() / cluster_centroid_ratio), random_state=0)
        print("Clustering Started.")
        clustering.fit(num_data)
        print("Clustering ended.")

        cluster_assignment = clustering.labels_

    all_clusters = dict()
    for j in range(len(cluster_assignment)):
        all_clusters.setdefault(cluster_assignment[j], []).append(num_data[j])

    cluster_to_label_dict = dict()
    for j in range(len(cluster_assignment)):
        if labels[j] != -1:
            cluster_to_label_dict.setdefault(cluster_assignment[j], []).append(labels[j])

    # normal labels removed
    # print("Clusters:")
    # cont_normal_count = 0
    # for k, v in cluster_to_label_dict.items():
    #     print(k)
    #     trYunique, trYcounts = np.unique(v, return_counts=True)
    #     pr_dict = dict()
    #     for i in range(len(trYunique)):
    #         if trYunique[i] == cat_dict['Normal'] and len(trYcounts) > 1:
    #             cont_normal_count += trYcounts[i]
    #         pr_dict[trYunique[i]] = trYcounts[i]
    #     pprint(pr_dict)
    # print("\n")
    # print(cont_normal_count)

    label_to_cluster_dict = dict()
    for k, v in cluster_to_label_dict.items():
        cl_labels, cl_label_counts = np.unique(np.array(v), return_counts=True)  # labeled member count in each cluster

        total_labeled_counts = np.sum(cl_label_counts)
        max_label = np.argmax(cl_label_counts)

        '''
        If a cluster contains more than 10% of all samples of a label present in the training dataset, it is considered 
        important for this label.
        If the majority label is not the normal label and there are no other labels for which this cluster is important,
        having more than 50% of the labeled members would be enough for soft labeling
        If the majority label is the normal label, then all labeled members must be normal for soft labeling.
        In any other case, we do not soft label.
        '''

        imp_for_label = []
        for label, total_label_count in total_original_label_counts.items():
            for j in range(len(cl_labels)):
                if cl_labels[j] == label and cl_label_counts[j] > 0.1 * total_label_count:
                    imp_for_label.append(label)

        if (cl_label_counts[max_label] / total_labeled_counts) > 0.5:
            selected_label = cl_labels[max_label]
            if len(imp_for_label) == 1:
                if imp_for_label[0] == selected_label:
                    if selected_label != int(cat_dict['Normal']):
                        label = selected_label
                        size = len(v)
                        label_to_cluster_dict.setdefault(label, []).append([k, size])
                    else:
                        if len(cl_labels) == 1:
                            label = selected_label
                            size = len(v)
                            label_to_cluster_dict.setdefault(label, []).append([k, size])
            elif len(imp_for_label) == 0:
                if selected_label != int(cat_dict['Normal']):
                    label = selected_label
                    size = len(v)
                    label_to_cluster_dict.setdefault(label, []).append([k, size])
                else:
                    if len(cl_labels) == 1:
                        label = selected_label
                        size = len(v)
                        label_to_cluster_dict.setdefault(label, []).append([k, size])

    '''
    clusters that belong to a particular label, after soft labeling.
    '''
    print("Cluster to Label Done.")
    soft_label_mapping = dict()
    for k, v in label_to_cluster_dict.items():
        for cluster_index in v:
            soft_label_mapping[cluster_index[0]] = k

    '''
    soft labeling particular unlabeled samples.
    Also add this to labeled dataset.
    '''
    print("Soft Label Mapping of Clusters Done.")

    new_samples = []
    new_samples_labels = []

    orignal_to_predicted_label_mapping = dict()  # original --> [predicted]
    total_correct_soft_labeling = 0
    total_incorrect_soft_labeling = 0
    total_unlabeled_samples = 0

    for j in range(len(labels)):
        if labels[j] == -1:
            total_unlabeled_samples += 1
        if labels[j] == -1 and (int(cluster_assignment[j]) in soft_label_mapping.keys()):
            labels[j] = soft_label_mapping[cluster_assignment[j]]
            orignal_to_predicted_label_mapping.setdefault(original_labels[j], []).append(labels[j])
            new_samples.append(num_data[j])
            new_samples_labels.append(labels[j])

            if labels[j] == original_labels[j]:
                total_correct_soft_labeling += 1
            else:
                total_incorrect_soft_labeling += 1

    if len(new_samples) > 0:
        labeled_dataset.add_sample(np.array(new_samples), np.array(new_samples_labels))

    '''
    checking total labeled and soft labeled members.
    '''
    if label_ratio != 1:
        print("Soft Labeling Done. ")
        print("Labeled Samples Percentage: {}%".format(label_ratio * 100))
        print("Total Samples Soft Labeled: {}".format(total_correct_soft_labeling + total_incorrect_soft_labeling))
        print("Total Correct Soft Labeling: {}".format(total_correct_soft_labeling))
        print("Total Incorrect Soft Labeling: {}".format(total_incorrect_soft_labeling))
        print("Total Soft Labeling Frequency: {}".format((total_correct_soft_labeling + total_incorrect_soft_labeling) /
                                                         total_unlabeled_samples))
        print("Soft Labeling Accuracy: {}".format(total_correct_soft_labeling /
                                                  (total_correct_soft_labeling + total_incorrect_soft_labeling)))

        print("\n\n")

    total_soft_label_counts = dict()
    distinct_slabels, distinct_slabel_counts = np.unique(labels, return_counts=True)
    for j in range(len(distinct_slabels)):
        if distinct_slabels[j] != -1:
            total_soft_label_counts[distinct_slabels[j]] = distinct_slabel_counts[j]

    print("Label Count After Soft Labeling: ")
    print(total_soft_label_counts)
    print("\n\n")

    cluster_to_labels_dict = dict()
    for k, v in cluster_to_label_dict.items():
        cl_labels = np.unique(np.array(v))
        cl_labels = sorted(list(cl_labels))
        if cl_labels[0] == -1:
            cl_labels = cl_labels[1:]
        cluster_to_labels_dict[k] = cl_labels

    total_dataset.set_y(labels)

    dt_X = labeled_dataset.get_x()
    dt_Y = labeled_dataset.get_y()

    print("labeled members dimensions:")
    print(dt_X.shape)
    print(dt_Y.shape)

    clf = DecisionTreeClassifier(random_state=0)
    clf.fit(dt_X, dt_Y)

    print("No. of leaves of decision tree:")
    print(clf.get_n_leaves())

    '''
    saving decision tree, cluster membership info (this is just to skip the clustering step for our faster use) and soft
    labeling info.
    '''

    file = open(os.path.join('models', 'models_' + str(method) + "_" + str(label_ratio), 'tree.pkl'), 'wb')
    pickle.dump(clf, file)
    file.close()

    if not load_cluster_from_file:
        file = open(os.path.join('clustering', 'nslkdd' + "_" + str(cluster_centroid_ratio) + ".pkl"), 'wb')
        pickle.dump(clustering, file)
        file.close()


def generate_result(test_X, label_ratio_, test_Y=None, path_to_model='models'):
    clf = pickle.load(
        file=open(os.path.join(path_to_model, 'models_' + str(method) + "_" + str(label_ratio_), 'tree.pkl'), 'rb'))

    # print("Predicting Leaf nodes...")
    test_Y_pred = clf.predict(test_X)

    if test_Y is not None:
        print(confusion_matrix(test_Y, test_Y_pred))
        print(classification_report(test_Y, test_Y_pred))

    return test_Y_pred


def train_model():
    if not os.path.exists('models'):
        os.mkdir('models')

    if not os.path.exists(os.path.join('models', 'models_' + str(method) + "_" + str(label_ratio))):
        os.mkdir(os.path.join('models', 'models_' + str(method) + "_" + str(label_ratio)))

    tree_work(load_cluster_from_file=True)

#
train_model()
total_dataset, labeled_dataset, unlabeled_dataset = get_training_data(no_samples=125973, label_ratio=1.0)
generate_result(total_dataset.get_x(), test_Y=total_dataset.get_y(), label_ratio_=label_ratio)
generate_result(test_dataset.get_x(), test_Y=test_dataset.get_y(), label_ratio_=label_ratio)
